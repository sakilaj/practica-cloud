# PRÁCTICAS MÓDULO CLOUD

[![pipeline status](https://gitlab.com/cursos-repo/lemoncode/cloud/practica-cloud/badges/master/pipeline.svg)](https://gitlab.com/cursos-repo/lemoncode/cloud/practica-cloud/commits/master) [![Docker Automated build](https://img.shields.io/docker/automated/lagp/practica-cloud.svg)](https://cloud.docker.com/u/lagp/repository/docker/lagp/practica-cloud) [![GitLab Registry](https://img.shields.io/badge/gitlab%20registry-manual-yellow.svg)](https://gitlab.com/cursos-repo/lemoncode/cloud/practica-cloud/container_registry)

## Preparación

* Cuenta de [Docker Hub](https://hub.docker.com/)
* Cuenta de [AWS](http://aws.amazon.com/)
* Haz un fork de este repositorio

## Práctica 1

Dado este proyecto en NodeJS, crea su Dockerfile sabiendo que nos han pedido como imagen base ubuntu:18.04, versión 10 de NodeJS, el 8888 será el puerto donde exponga la comunicación la applicación, la señal de *STOP* debe llegarle a la aplicación y el contenedor podría ser iniciado con cualquier proceso.

## Práctica 2

Sube la imagen de Docker a DockerHub.

## Práctica 3

Automatiza el proceso de creación de la imagen de Docker y su subida a Docker Hub después de cada cambio en el repositorio.

## Práctica 4

Crea un servidor y despliega la imagen de Docker en AWS utilizando Terraform.

## Comentarios sobre la resolución de la práctica

He decidido utilizar GitLab para realizar la práctica porque no se necesita un servicio externo para realizar tareas de integración continua. Además, he aprovechado la ocasión para probar a subir la imagen de Docker al registry del repositorio de GitLab.

La ejecución de la integración continua realiza las siguientes acciones:

1. Creación y subida de la imagen de Docker al registry de GitLab.[^1]
2. Creación y subida de la imagen de Docker al registry de DockerHub.[^1]
3. Despliegue de la infraestructura necesaria en Amazon Web Services mediante Terraform.
4. Despliegue de la aplicación en Amazon Web Services.

La imagen de Docker generada en la práctica está disponible en los siguientes registries:

* [GitLab](https://gitlab.com/cursos-repo/lemoncode/cloud/practica-cloud/container_registry)
* [Docker Hub](https://hub.docker.com/r/lagp/practica-cloud)

Para conocer el estado de la última ejecución de la integración continua pulsar [aquí](https://gitlab.com/cursos-repo/lemoncode/cloud/practica-cloud/pipelines/53138217)

### Creación y subida de la imagen de Docker al registry de GitLab [^1]

Se ha definido una tarea durante la integración continua que construye y sube una imagen de Docker con nuestra aplicación al registry de GitLab.

El código que genera dicha imagen se puede consultar en el archivo [.gitlab-ci.yml](.gitlab-ci.yml) bajo la tarea `1/2 GitLab`.

```yml
1/2 Gitlab:
  image: docker:stable
  stage: registry
  services:
    - docker:dind
  only:
    - master
  script:
    - echo "$GITLAB_REGISTRY_PASSWORD" | docker login registry.gitlab.com -u ${GITLAB_REGISTRY_USER} --password-stdin
    - docker build -t registry.gitlab.com/cursos-repo/lemoncode/cloud/practica-cloud .
    - docker push registry.gitlab.com/cursos-repo/lemoncode/cloud/practica-cloud
```

Las credenciales necesarias para realizar el login en el registry de GitLab se encuentran almacenadas en las siguientes variables de entorno:

* `$GITLAB_REGISTRY_USER`: Contiene el usuario de conexión.
* `$GITLAB_REGISTRY_PASSWORD`: Contiene la contraseña asociada al usuario.

El uso de las variables de entorno anteriores nos evita la exposición de las credenciales de acceso al registry de GitLab.

Para ampliar la información sobre la construcción de la imagen de Docker, consultar el archivo [Dockerfile](Dockerfile).

Para ver el proceso de creación de la imagen consultar la tarea [#182441333](https://gitlab.com/cursos-repo/lemoncode/cloud/practica-cloud/-/jobs/182441333)

### Creación y subida de la imagen de Docker al registry de DockerHub [^1]

El proceso de creación de la imagen de Docker y la subida al registry de Docker Hub es muy similar al proceso anterior donde la subida se realiza al registry de GitLab. La única diferencia está en la subida de la imagen al registry.

El código que genera la imagen de Docker se puede consultar en el archivo [.gitlab-ci.yml](.gitlab-ci.yml) bajo la tarea `2/2 Docker Hub`.

```yml
2/2 Docker Hub:
  image: docker:stable
  stage: registry
  services:
    - docker:dind
  only:
    - master
  script:
    - echo "$DOCKER_HUB_PASSWORD" | docker login -u ${DOCKER_HUB_USER} --password-stdin
    - docker build -t lagp/practica-cloud .
    - docker push lagp/practica-cloud
```

El uso de variables de entorno evita exponer las credenciales de acceso al registry de Docker Hub:

* `$DOCKER_HUB_USER`: Contiene el usuario de conexión.
* `$DOCKER_HUB_PASSWORD`: Contiene la contraseña asociada al usuario.

Para ampliar la información sobre la construcción de la imagen de Docker, consultar el archivo [Dockerfile](Dockerfile).

Para ver el proceso de creación de la imagen consultar la tarea [#182441334](https://gitlab.com/cursos-repo/lemoncode/cloud/practica-cloud/-/jobs/182441334)

### Despliegue de la infraestructura necesaria en Amazon Web Services mediante Terraform

Mediante el uso de [Terraform](https://www.terraform.io/) podemos especificar y desplegar la infraestructura necesaria en Amazon Web Services para desplegar nuestra aplicación.

El archivo [main.tf](deployment/main.tf) contiene la configuración de la instancia que se desplegará en Amazon Web Services.

El proceso de despligue se ha divido en varias tareas que se ejecutan durante la integración continua:

1. Validación de la sintaxis del archivo de configuración de Terraform.[^2]

    ```yml
    1/2 validate:
    image:
        name: hashicorp/terraform:0.11.3
        entrypoint: ["/bin/sh", "-c"]
    stage: terraform plan
    before_script:
        - cd deployment
        - terraform -version
        - terraform init
    script:
        - terraform validate
    ```

   Examinar la tarea [#182441335](https://gitlab.com/cursos-repo/lemoncode/cloud/practica-cloud/-/jobs/182441335) para ver el resultado de su ejecución.  

2. Creación del plan de ejecución.[^2]

   ```yml
   2/2 plan:
    image:
        name: hashicorp/terraform:0.11.3
        entrypoint: ["/bin/sh", "-c"]
    stage: terraform plan
    before_script:
        - cd deployment
        - terraform -version
    script:
        - terraform init
        - terraform plan -input=false -out=production_plan.bin
    artifacts:
        paths:
        - deployment/production_plan.bin
   ```

   Examinar la tarea [#182441336](https://gitlab.com/cursos-repo/lemoncode/cloud/practica-cloud/-/jobs/182441336) para ver el resultado de su ejecución.

3. Aplicación del plan de ejecución.

   ```yml
   1/1 apply:
    image:
        name: hashicorp/terraform:0.11.3
        entrypoint: ["/bin/sh", "-c"]
    stage: terraform apply
    before_script:
        - cd deployment
        - terraform -version
    script:
        - terraform init
        - terraform apply -auto-approve -input=false production_plan.bin
        - terraform output > deploy-server-ip.txt
    dependencies:
        - 2/2 plan
    artifacts:
        paths:
        - deployment/terraform.tfstate
        - deployment/deploy-server-ip.txt
    when: manual
   ```

   Esta tarea de aplicación del plan de ejecución se ejecuta de manera manual.

   Al finalizar la aplicación del plan de ejecución se muestra la dirección Ip que ha sido asignada a la instancia de Amazon Web Service. Necesitamos esta dirección IP para poder realizar el despliegue de la aplicación posteriormente.

   Examinar la tarea [#182441337](https://gitlab.com/cursos-repo/lemoncode/cloud/practica-cloud/-/jobs/182441337) para ver el resultado de su ejecución.

4. Destrucción de la instancia en Amazon Web Services.

   ```yml
   1/1 terraform:destroy:
    image:
       name: hashicorp/terraform:0.11.3
       entrypoint: ["/bin/sh", "-c"]
    stage: terraform destroy
    before_script:
        - cd deployment
        - terraform -version
    script:
        - terraform init
        - terraform destroy -input=false -force -target="aws_instance.practica_cloud_server" -target="aws_security_group.aws_practica_cloud"
    dependencies:
        - 1/1 apply
    when: manual
   ```

   Esta tarea de destrucción de la instancia de Amazon Web Services se ejecuta de manera manual.

### Despliegue de la aplicación en Amazon Web Services

El despliegue de la aplicación en la instancia de Amazon se realiza mediante una conexión SSH y la ejecución de los comandos necesarios para iniciar un contenedor Docker con la imagen que se ha creado en la tarea anterior. En este caso, se utilizará la imagen ubicada en el registry de GitLab, pero se podría haber utilizado la imagen ubicada en Docker Hub.

Esta tarea depende del despliegue previo de la infraestructura en Amazon Web Service, por lo que la ejecución de la misma se ha de realizar de manera manual.

Las instrucciones necesarias para desplegar la aplicación se pueden consultar en el archivo [.gitlab-ci.yml](.gitlab-ci.yml) bajo la tarea `1/1 deploy`.

```yml
1/1 deploy:
  image: ubuntu
  stage: cd
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client git -y )'
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$PRIVATE_KEY")
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - AWS_SERVER_IP=$(sed -n '1p' ./deployment/deploy-server-ip.txt | cut -c 12-)
    - ssh-keyscan ${AWS_SERVER_IP}  >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  script:
    - ssh ec2-user@${AWS_SERVER_IP} "sudo yum install docker -y &&
                         sudo service docker start && 
                         echo "$GITLAB_REGISTRY_ACCESS_TOKEN" | sudo docker login -u ${GITLAB_REGISTRY_USER} --password-stdin registry.gitlab.com && 
                         sudo docker run --rm -d -p 80:8888 --name practica-cloud registry.gitlab.com/cursos-repo/lemoncode/cloud/practica-cloud"
    - echo "Deploy on http:\\\\$AWS_SERVER_IP"
  dependencies:
    - 1/1 apply
  when: manual
```

Para ver el proceso de despliegue de la aplicación consultar la tarea [#182441339](https://gitlab.com/cursos-repo/lemoncode/cloud/practica-cloud/-/jobs/182441339)

### Acceso a la aplicación en Amazon Web Services

Para acceder a la aplicación utilizar la siguiente dirección:

[http://3.17.4.143](http://3.17.4.143)

[^1]: Las tareas de creación de la imagen de Docker y su subida a los registries de GitLab y Docker Hub se ejecutan de manera simultanea.

[^2]: Las tareas de comprobación de la sintaxis del archivo de configuración de Terraform y la creación del plan de ejecución se ejecutan de manera simultanea.